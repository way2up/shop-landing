<?php

if(isset($_POST['email'])){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $to = "contact@way2up.am";
    $subject = 'Shop admin panel request';
    $headers = 'From:info@way2up.am'. "\r\n" .
                'Reply-To:info@way2up.am'. "\r\n" .
                'X-Mailer: PHP/' . phpversion();

    $body = <<<EMAIL

    Hi Way2Up team!
    
    We have new request from $name.
    
    His/Her email is $email.
    
    Need to create test admin panel for this user.
    
    Kind Regards
EMAIL;

    if(mail($to, $subject, $body, $headers)){
        echo true;
    }else{
        echo false;
    }
}
